# Covalent Mod

Modified version of Covalent theme by [Gerson Lázaro](http://www.gersonlazaro.com). Fixed to work with Ghost 1.5, and slight changes made here and there, by [sehro](https://sehro.org).

Changes:
*   Disqus removed (I don't do comments. Ever.)
*   Image tags fixed to work with Ghost 1+
